﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SerializeDatatables.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            var Param = new
            {
                FirstName = "john",
                LastName = "Shehata"
            };
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult datatable()
        {
            return View();
        }


        [HttpPost]
        public ActionResult GetData()
        {
            Data.Repository RepoObj = new Data.Repository();
            DataTable Dt = RepoObj.GetEmployees();
            var Result = Newtonsoft.Json.JsonConvert.SerializeObject(Dt);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }


    }
}