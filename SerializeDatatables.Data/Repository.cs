﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerializeDatatables.Data
{
    public class Repository
    {
        public DataTable GetEmployees()
        {
            SqlConnection Conn = null;
            SqlCommand Cmd = null;
            SqlDataAdapter Adapt = null;
            DataTable Dt = new DataTable();
            try
            {
                Conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["NORTHWNDEntities"].ToString());
                Conn.Open();
                Cmd = Conn.CreateCommand();
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "select FirstName,LastName,Title from employees";
                Adapt = new SqlDataAdapter(Cmd);
                Adapt.Fill(Dt);
            }
            catch(Exception ex)
            {
                Dt = null;

            }
            finally
            {
                if (Adapt != null)
                    Adapt.Dispose();

                if (Cmd != null)
                    Cmd.Dispose();

                if (Conn != null)
                    Conn.Dispose();
            }
            return Dt;
        }
    }
}
